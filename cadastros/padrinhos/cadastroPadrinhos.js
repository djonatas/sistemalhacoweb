'use strict';

angular.module('myApp.cadastroPadrinhos', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/padrinhos/novo', {
            templateUrl: 'cadastros/padrinhos/cadastroPadrinhos.html',
            controller: 'ViewCadastroPadrinhosController'
        });
    }])

    .controller('ViewCadastroPadrinhosController', ['$scope', '$http', function($scope, $http) {


        $scope.data = {
            humano: 'Humano',
            palhaco: '',
            cpf: '',
            rg: '',
            endereco: '',
            complemento: '',
            bairro: '',
            CEP: '',
            cidade: '',
            estado: '',
            telResidencial: '',
            celular: '',
            profissao: '',
            empresa: '',
            email: '',
            central: 0,
            dataAniversario: new Date(),
            dataEntradaONG: new Date(),
            dataCadastro: new Date(),
            padrinho: 1
        };

        $scope.saveGodfatherAngular = function (){
            var req = {
                method: 'POST',
                url: 'https://presentealegriaapi.herokuapp.com/godfather',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.data
            };

            $http(req)
                .then(function(data){
                    console.log(data);
                }, function(error){
                    console.log(error);
                    console.log('ERROR');
                });
        };
    }]);