'use strict';

angular.module('myApp.pesquisarPadrinhos', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/padrinhos/busca', {
            templateUrl: 'cadastros/padrinhos/buscaPadrinhos.html',
            controller: 'ViewPesquisarPadrinhosController'
        });
    }])

    .controller('ViewPesquisarPadrinhosController', [function() {

    }]);